﻿Public Class Window1
    Public n1 As Boolean = False
    Public n2 As Boolean = False
    Public n3 As Boolean = False
    Public n4 As Boolean = False
    Public n5 As Boolean = False
    Public n6 As Boolean = False
    Public nm1 As String
    Public nm2 As String
    Public nm3 As String
    Public nm4 As String
    Public nm5 As String
    Public nm6 As String
    Public afirmativo As Integer
    Public de6A3 As Boolean = False
    Public cantNombres As Integer = 0
    Public contadorVez As Integer = 0
 
    Public Sub txtNP1_TextChanged(sender As Object, e As TextChangedEventArgs) Handles txtNP1.TextChanged
        ' txtNP1.Text = ""
    End Sub
    Private Sub txtNP1_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP1.MouseDoubleClick
        txtNP1.Text = ""
    End Sub
    Private Sub txtNP2_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP2.MouseDoubleClick
        txtNP2.Text = ""
    End Sub
    Private Sub txtNP3_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP3.MouseDoubleClick
        txtNP3.Text = ""
    End Sub
    Private Sub txtNP4_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP4.MouseDoubleClick
        txtNP4.Text = ""
    End Sub
    Private Sub txtNP5_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP5.MouseDoubleClick
        txtNP5.Text = ""
    End Sub
    Private Sub txtNP6_DoubleClick(sender As Object, e As MouseEventArgs) Handles txtNP6.MouseDoubleClick
        txtNP6.Text = ""
    End Sub
    Private Sub chk1_Checked(sender As Object, e As RoutedEventArgs) Handles chk1.Checked
        If Not (txtNP1.Text = "" Or txtNP1.Text = " ") Then
            n1 = True
            cantNombres += 1
        Else
            MsgBox("Faltan Datos! D:")
            chk1.IsChecked() = False
            txtNP1.Text = "Player1"
            n1 = False
        End If
    End Sub
    Private Sub chk2_Checked(sender As Object, e As RoutedEventArgs) Handles chk2.Checked
        If Not (txtNP2.Text = "" Or txtNP2.Text = " ") Then
            n2 = True
            cantNombres += 1

        Else
            MsgBox("Faltan Datos! D:")
            chk2.IsChecked() = False
            txtNP2.Text = "Player2"
            n2 = False
        End If
    End Sub
    Private Sub chk3_Checked(sender As Object, e As RoutedEventArgs) Handles chk3.Checked
        If Not (txtNP3.Text = "" Or txtNP3.Text = " ") Then
            cantNombres += 1
            n3 = True
        Else
            MsgBox("Faltan Datos! D:")
            chk3.IsChecked() = False
            txtNP3.Text = "Player3"
            n3 = False
        End If
    End Sub
    Private Sub chk4_Checked(sender As Object, e As RoutedEventArgs) Handles chk4.Checked
        If Not (txtNP4.Text = "" Or txtNP4.Text = " ") Then
            n4 = True
            cantNombres += 1
        Else
            MsgBox("Faltan Datos! D:")
            chk4.IsChecked() = False
            txtNP4.Text = "Player4"
            n4 = False
        End If
    End Sub
    Private Sub chk5_Checked(sender As Object, e As RoutedEventArgs) Handles chk5.Checked
        If Not (txtNP5.Text = "" Or txtNP5.Text = " ") Then
            n5 = True
            cantNombres += 1
        Else
            MsgBox("Faltan Datos! D:")
            chk5.IsChecked() = False
            txtNP5.Text = "Player5"
            n5 = False
        End If
    End Sub
    Private Sub chk6_Checked(sender As Object, e As RoutedEventArgs) Handles chk6.Checked
        If Not (txtNP6.Text = "" Or txtNP6.Text = " ") Then
            n6 = True
            cantNombres += 1
        Else
            MsgBox("Faltan Datos! D:")
            chk6.IsChecked() = False
            txtNP6.Text = "Player6"
            n6 = False
        End If
    End Sub
    Private Sub Listo_Click(sender As Object, e As RoutedEventArgs) Handles Listo.Click
        If de6A3 = False Then
            If Not (txtNP1.Text = "") And Not (txtNP2.Text = "") And Not (txtNP3.Text = "") And Not (txtNP4.Text = "") And Not (txtNP5.Text = "") And Not (txtNP6.Text = "") Then
                'If n1 And n2 And n3 And n4 And n5 And n6 Then
                Me.nm1 = txtNP1.Text
                Me.nm2 = txtNP2.Text
                Me.nm3 = txtNP3.Text
                Me.nm4 = txtNP4.Text
                Me.nm5 = txtNP5.Text
                Me.nm6 = txtNP6.Text
                Me.afirmativo = 1
                Me.Close()
            Else
                MsgBox("Necesitamos los nombres para Jugar!")
                Me.afirmativo = 3
            End If
        ElseIf de6A3 = True Then
            If cantNombres Mod 3 = 0 Then
                If n1 = True Then
                    nm1 = txtNP1.Text
                End If

                If n2 = True Then
                    nm2 = txtNP2.Text
                End If
                If n3 = True Then
                    nm3 = txtNP3.Text
                End If
                If n4 = True Then
                    nm4 = txtNP4.Text
                End If
                If n5 = True Then
                    nm5 = txtNP5.Text
                End If
                If n6 = True Then
                    nm6 = txtNP6.Text
                End If
                Me.afirmativo = 1
                Me.Close()
            Else
                MsgBox("Solo 3 Jugadores")
            End If
        End If
    End Sub

    Private Sub CambioA3_Click(sender As Object, e As RoutedEventArgs) Handles CambioA3.Click
        If contadorVez Mod 2 = 0 Then
            MsgBox("Selecciona a los tres Jugadores!")
            chk1.Visibility = Windows.Visibility.Visible
            chk2.Visibility = Windows.Visibility.Visible
            chk3.Visibility = Windows.Visibility.Visible
            chk4.Visibility = Windows.Visibility.Visible
            chk5.Visibility = Windows.Visibility.Visible
            chk6.Visibility = Windows.Visibility.Visible
            de6A3 = True
            CambioA3.Content = "Cambio a 6 Jugadores"
        Else
            MsgBox("Selecciona a los seis Jugadores!")
            chk1.Visibility = Windows.Visibility.Hidden
            chk2.Visibility = Windows.Visibility.Hidden
            chk3.Visibility = Windows.Visibility.Hidden
            chk4.Visibility = Windows.Visibility.Hidden
            chk5.Visibility = Windows.Visibility.Hidden
            chk6.Visibility = Windows.Visibility.Hidden
            de6A3 = False
            CambioA3.Content = "Cambio a 3 Jugadores"
        End If
        contadorVez += 1
    End Sub
    Private Sub chk1_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk1.Unchecked
        n1 = False
        cantNombres = (cantNombres - 1)
    End Sub
    Private Sub chk2_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk2.Unchecked
        n2 = False
        cantNombres = (cantNombres - 1)
    End Sub
    Private Sub chk3_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk3.Unchecked
        n3 = False
        cantNombres = (cantNombres - 1)
    End Sub
    Private Sub chk4_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk4.Unchecked
        n4 = False
        cantNombres = (cantNombres - 1)
    End Sub
    Private Sub chk5_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk5.Unchecked
        n5 = False
        cantNombres = (cantNombres - 1)
    End Sub
    Private Sub chk6_Unchecked(sender As Object, e As RoutedEventArgs) Handles chk6.Unchecked
        n6 = False
        cantNombres = (cantNombres - 1)
    End Sub
End Class
