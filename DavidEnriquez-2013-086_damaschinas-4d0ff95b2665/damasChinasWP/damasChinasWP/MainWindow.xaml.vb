﻿Imports System
Imports System.Collections
Imports Microsoft.VisualBasic
Class MainWindow
    Dim coordenadasX() = {133.33, 200, 333.33}
    Dim coordenadasY() = {0, 200, 215, 400}
    Dim anchoRuedita As Integer = 10
    Dim numero As Integer = 0
    Dim contador As Double = 0
    Dim numerox As Integer = 0
    Dim numerox2 As Integer = 0
    Dim contadorTamanio As Integer = 0
    Dim ventanaNombres6 As New Window1
    Dim ventanaNombres3 As New Nombres3
    Dim enfrente As Boolean
    Dim contadorAgregar As Integer = 0

    Dim listaMorado As New ArrayList
    Dim listaAmarillo As New ArrayList
    Dim listaNaranja As New ArrayList
    Dim listaRojo As New ArrayList
    Dim listaAzul As New ArrayList
    Dim listaVerde As New ArrayList

    Public contadorUniversal As Integer = 0
    Dim juego6 As Boolean
    Dim juego3 As Boolean

    Dim mode3 As Boolean = False
    ' Controles
    Dim fichas(59) As Object
    Dim posicionesXFichas(59) As Integer
    Dim posicionesYFichas(59) As Integer

    Dim espacios(120) As Ellipse
    Dim posicionesXEspacios(120) As Integer
    Dim posicionesYEspacios(120) As Integer
    Dim espaciosPosibles(0) As Object
    Dim haciaLaDerecha As Integer = 40
    Dim haciaLaIzquierda As Integer = 40 + anchoRuedita

    Dim K As New Point
    Dim L As New Point
    Dim Ñ As New Point
    Dim ficha As New Object
    Dim ficha2 As New Object
    Dim ficha3 As New Object
    
    Private Sub mostrarJugadores()
        JugadoresTitle.Visibility() = True
        lblp1.Visibility() = True
        lblp2.Visibility() = True
        lblp3.Visibility() = True
        lblp4.Visibility() = True
        lblp5.Visibility() = True
        lblp6.Visibility() = True
    End Sub
    Private Sub ocultarJugadores()
        JugadoresTitle.Visibility() = False
        lblp1.Visibility() = False
        lblp2.Visibility() = False
        lblp3.Visibility() = False
        lblp4.Visibility() = False
        lblp5.Visibility() = False
        lblp6.Visibility() = False
    End Sub
    'manda a llamar al metodo que Carga los espacios en blanco de las 6 puntas
    Private Sub Tablero()
        Dim espacio As New Espacio
        'Norte
        casse(0, 3, pipo.Width / 2, 25, 1, espacio.Blanco.Fill)
        'NorEste
        casse(3, 0, pipo.Width - (pipo.Width / 6), 165, -1, espacio.Blanco.Fill)
        'NotOeste
        casse(3, 0, pipo.Width / 6, 165, -1, espacio.Blanco.Fill)
        'Sur
        casse(3, 0, pipo.Width / 2, 345, -1, espacio.Blanco.Fill)
        'SurEste
        casse(0, 3, (pipo.Width - (pipo.Width / 6)), 205, 1, espacio.Blanco.Fill)
        'SurOeste
        casse(0, 3, pipo.Width / 6, 205, 1, espacio.Blanco.Fill)
    End Sub
    'Habilita y prepara el tablero para 6 jugadores
    Private Sub Modo6()
        juego6 = True
        If ventanaNombres3.IsVisible() Then
            ventanaNombres3.Close()
        End If
        If ventanaNombres6.IsVisible() Then
            ventanaNombres6.Close()
        End If
        ventanaNombres6 = New Window1
        ventanaNombres6.Show()
        Dim fichaRoja As New rojo
        Dim fichaAmarilla As New Amarillo
        Dim fichaMorda As New Morado
        Dim fichaAzul As New Azul
        Dim fichaNaranja As New Naranja
        Dim fichaVerde As New Verde
        ''FICHAS!!
        'Norte
        cargarFichasMorada(0, 3, pipo.Width / 2, 25, 1)
        'NorEste
        cargarFichasAmarillas(3, 0, pipo.Width - (pipo.Width / 6), 165, -1)
        'NorOeste
        cargarFichasVerdes(3, 0, (pipo.Width / 6), 165, -1)
        'Sur
        cargarFichasRojos(3, 0, pipo.Width / 2, 345, -1)
        'SurEste
        cargarFichasNaranja(0, 3, (pipo.Width - (pipo.Width / 6)), 205, 1)
        'SurOeste
        cargarFichasAzul(0, 3, (pipo.Width / 6), 205, 1)
        _3P.IsEnabled = False
        _6P.IsEnabled = False

    End Sub
    'Prepara el tablero para 3 jugadores
    Private Sub Modo3()
        juego3 = True

        If ventanaNombres6.IsVisible() Then
            ventanaNombres6.Close()
        End If
        If ventanaNombres3.IsVisible() Then
            ventanaNombres3.Close()
        End If
        ventanaNombres3 = New Nombres3
        ventanaNombres3.Show()
        Dim fichaMorda As New Morado
        Dim fichaNaranja As New Naranja
        Dim fichaVerde As New Azul
        mode3 = True
        pipo.Children.Clear()
        ReDim ficha(45)
        numerox2 = 0
        numerox = 0
        numero = 0
        ReDim espacios(166)
        ReDim posicionesXEspacios(166)
        ReDim posicionesYEspacios(166)
        ReDim posicionesXFichas(45)
        ReDim posicionesYFichas(45)
        Tablero()
        hexagono()
        _3P.IsEnabled = False
        _6P.IsEnabled = False
        'Norte
        cargarFichasMorada(0, 3, pipo.Width / 2, 25, 1)
        'SurEste
        cargarFichasNaranja(0, 3, (pipo.Width - (pipo.Width / 6)), 205, 1)
        'SurOeste
        cargarFichasAzul(0, 4, pipo.Width / 6, 205, 1)
    End Sub
    '"vacia" o "reinicia" todos los valores para tenerlos como nuevos ante un nuevo movimiento
    Private Sub reiniciar()
        juego3 = False
        juego6 = False

        ventanaNombres6.afirmativo = 5
        ventanaNombres3.de3A6 = False
        ventanaNombres6.de6A3 = False
        mode3 = False
        contadorAgregar = 0
        pipo.Children.Clear()
        numerox2 = 0
        numerox = 0
        numero = 0
        ReDim fichas(59)
        ReDim espacios(120)
        ReDim posicionesXEspacios(120)
        ReDim posicionesYEspacios(120)
        ReDim posicionesXFichas(59)
        ReDim posicionesYFichas(59)
        Tablero()
        hexagono()
        _3P.IsEnabled = True
        _6P.IsEnabled = True
    End Sub
    'metodo que carga las piezas blancas de las puntas
    Public Sub casse(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer, ByRef color As SolidColorBrush)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Ellipse
            fichax = New Ellipse
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            fichax.Width = 20 : fichax.Height = 20 : fichax.Stroke = Brushes.Black
            fichax.Fill = color
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Ellipse
                            fichax1.Width = 20 : fichax1.Height = 20 : fichax1.Fill = color : fichax1.Stroke = Brushes.Black : fichax1.StrokeMiterLimit = 2
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            espacios(numero) = fichax1
                            posicionesXEspacios(numero) = Canvas.GetLeft(fichax1)
                            posicionesYEspacios(numero) = Canvas.GetTop(fichax1)
                            pipo.Children.Add(fichax1)
                            numero += 1
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Ellipse
                            fichax2.Width = 20 : fichax2.Height = 20 : fichax2.Fill = color : fichax2.Stroke = Brushes.Black
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            espacios(numero) = fichax2
                            posicionesXEspacios(numero) = Canvas.GetLeft(fichax2)
                            posicionesYEspacios(numero) = Canvas.GetTop(fichax2)
                            pipo.Children.Add(fichax2)
                            numero += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Ellipse
                            fichax3.Width = 20 : fichax3.Height = 20 : fichax3.Fill = color : fichax3.Stroke = Brushes.Black
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            espacios(numero) = fichax3
                            posicionesXEspacios(numero) = Canvas.GetLeft(fichax3)
                            posicionesYEspacios(numero) = Canvas.GetTop(fichax3)
                            pipo.Children.Add(fichax3)
                            numero += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y - 0))
                Else
                    Canvas.SetTop(fichax, (y + 0))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
                pipo.Children.Add(fichax)
                espacios(numero) = fichax
                posicionesXEspacios(numero) = Canvas.GetLeft(fichax)
                posicionesYEspacios(numero) = Canvas.GetTop(fichax)
                numero += 1
            End If
        Next
    End Sub
    'carga las fichas verdes
    Public Sub cargarFichasVerdes(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Verde
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Verde
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            listaVerde.Add(fichax1)
                            fichas(numerox) = fichax1
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                            pipo.Children.Add(fichax1)
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Verde
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            listaVerde.Add(fichax2)
                            fichas(numerox) = fichax2
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                            pipo.Children.Add(fichax2)
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Verde
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            listaVerde.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                            pipo.Children.Add(fichax3)
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                pipo.Children.Add(fichax)
                listaVerde.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'carga las fichas rojas
    Public Sub cargarFichasRojos(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New rojo
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New rojo
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            pipo.Children.Add(fichax1)
                            fichas(numerox) = fichax1
                            listaRojo.Add(fichax1)
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New rojo
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            listaRojo.Add(fichax2)
                            pipo.Children.Add(fichax2)
                            fichas(numerox) = fichax2
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New rojo
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            pipo.Children.Add(fichax3)
                            listaRojo.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                pipo.Children.Add(fichax)
                listaRojo.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'carga las fichas amarillas
    Public Sub cargarFichasAmarillas(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Amarillo
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Amarillo
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            listaAmarillo.Add(fichax1)
                            fichas(numerox) = fichax1
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                            pipo.Children.Add(fichax1)
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Amarillo
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            listaAmarillo.Add(fichax2)
                            pipo.Children.Add(fichax2)
                            fichas(numerox) = fichax2
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Amarillo
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            pipo.Children.Add(fichax3)
                            listaAmarillo.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                pipo.Children.Add(fichax)
                listaAmarillo.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'carga las fichas azules
    Public Sub cargarFichasAzul(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Azul
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            'fichax.AddHandler
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Azul
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            fichas(numerox) = fichax1
                            listaAzul.Add(fichax1)
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                            pipo.Children.Add(fichax1)
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Azul
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            listaAzul.Add(fichax2)
                            pipo.Children.Add(fichax2)
                            fichas(numerox) = fichax2
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Azul
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            listaAzul.Add(fichax3)
                            pipo.Children.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                listaAzul.Add(fichax)
                pipo.Children.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'carga las fichas moradas
    Public Sub cargarFichasMorada(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Morado
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Morado
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            pipo.Children.Add(fichax1)
                            listaMorado.Add(fichax1)
                            fichas(numerox) = fichax1
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Morado
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            pipo.Children.Add(fichax2)
                            fichas(numerox) = fichax2
                            listaMorado.Add(fichax2)
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Morado
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            listaMorado.Add(fichax3)
                            pipo.Children.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                listaMorado.Add(fichax)
                pipo.Children.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'carga las fichas narajas
    Public Sub cargarFichasNaranja(ByRef vueltax As Integer, ByRef fin As Integer, ByRef x As Integer, ByRef y As Integer, ByRef limite As Integer)
        For vuelta = vueltax To fin Step limite
            Dim fichax As New Naranja
            AddHandler fichax.MouseLeftButtonDown, AddressOf azulito
            AddHandler fichax.MouseLeftButtonDown, AddressOf radio
            If vuelta >= 1 Then
                Select Case vuelta
                    Case 1
                        For a = 0 To 1
                            Dim fichax1 As New Naranja
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax1.MouseLeftButtonDown, AddressOf radio
                            If 0 > limite Then
                                Canvas.SetTop(fichax1, (y - 20))
                            Else
                                Canvas.SetTop(fichax1, (y + 20))
                            End If
                            If a = 0 Then
                                Canvas.SetLeft(fichax1, x - 15 - anchoRuedita)
                            ElseIf a = 1 Then
                                Canvas.SetLeft(fichax1, x + 15 - anchoRuedita)
                            End If
                            listaNaranja.Add(fichax1)
                            pipo.Children.Add(fichax1)
                            fichas(numerox) = fichax1
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax1))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax1))
                            numerox += 1
                        Next
                    Case 2
                        For b = 0 To 2
                            Dim fichax2 As New Naranja
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax2.MouseLeftButtonDown, AddressOf radio
                            If 0 > limite Then
                                Canvas.SetTop(fichax2, (y - 40))
                            Else
                                Canvas.SetTop(fichax2, (y + 40))
                            End If
                            If b = 0 Then
                                Canvas.SetLeft(fichax2, x - 30 - anchoRuedita)
                            ElseIf b = 1 Then
                                Canvas.SetLeft(fichax2, x - anchoRuedita)
                            ElseIf b = 2 Then
                                Canvas.SetLeft(fichax2, x + 30 - anchoRuedita)
                            End If
                            pipo.Children.Add(fichax2)
                            listaNaranja.Add(fichax2)
                            fichas(numerox) = fichax2
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax2))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax2))
                            numerox += 1
                        Next
                    Case 3
                        For c = 0 To 3
                            Dim fichax3 As New Naranja
                            If 0 > limite Then
                                Canvas.SetTop(fichax3, (y - 60))
                            Else
                                Canvas.SetTop(fichax3, (y + 60))
                            End If
                            If c = 0 Then
                                Canvas.SetLeft(fichax3, x - 45 - anchoRuedita)
                            ElseIf c = 1 Then
                                Canvas.SetLeft(fichax3, x - 15 - anchoRuedita)
                            ElseIf c = 2 Then
                                Canvas.SetLeft(fichax3, x + 15 - anchoRuedita)
                            ElseIf c = 3 Then
                                Canvas.SetLeft(fichax3, x + 45 - anchoRuedita)
                            End If
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf azulito
                            AddHandler fichax3.MouseLeftButtonDown, AddressOf radio
                            listaNaranja.Add(fichax3)
                            pipo.Children.Add(fichax3)
                            fichas(numerox) = fichax3
                            posicionesXFichas(numerox) = (Canvas.GetLeft(fichax3))
                            posicionesYFichas(numerox) = (Canvas.GetTop(fichax3))
                            numerox += 1
                        Next
                End Select
            Else
                If 0 > limite Then
                    Canvas.SetTop(fichax, (y))
                Else
                    Canvas.SetTop(fichax, (y))
                End If
                Canvas.SetLeft(fichax, x - anchoRuedita)
                pipo.Children.Add(fichax)
                listaNaranja.Add(fichax)
                fichas(numerox) = fichax
                posicionesXFichas(numerox) = (Canvas.GetLeft(fichax))
                posicionesYFichas(numerox) = (Canvas.GetTop(fichax))
                numerox += 1
            End If
        Next
    End Sub
    'debugueo personal para verificar que no hallan errores en los arrays
    Public Sub verificarNotNullEspacios()
        For Each posicion As Double In posicionesXEspacios
            MsgBox("posiciones en x : " & posicion)
        Next
        For Each pos As Double In posicionesYEspacios
            MsgBox("posiciones en y espacios : " & pos)
        Next
        For Each posicion As Double In posicionesXFichas
            MsgBox("posiciones en x Fichas : " & posicion)
        Next
        For Each pos As Double In posicionesYFichas
            MsgBox("posiciones en y Fichas : " & pos)
        Next
    End Sub
    'carga el hexagono del centro
    Private Sub hexagono()
        Dim contador As Integer = 0
        Dim y As Integer = 85
        Dim x As Integer = (-15)
        For vezY = 0 To 8
            contador += 1
            y += 20
            x += 15
            'Primera fila
            If vezY = 0 Then
                generadorLineaBlanca(0, vezY + 4, x, y)
            ElseIf vezY = 1 Then
                generadorLineaBlanca(0, vezY + 4, x, y)
            ElseIf vezY = 2 Then
                generadorLineaBlanca(0, vezY + 4, x, y)
            ElseIf vezY = 3 Then
                generadorLineaBlanca(0, vezY + 4, x, y)
            ElseIf vezY = 4 Then
                generadorLineaBlanca(0, vezY + 4, x, y)
            ElseIf vezY = 5 Then
                generadorLineaBlanca(vezY + 2, 0, 45, y, -1)
            ElseIf vezY = 6 Then
                generadorLineaBlanca(vezY, 0, 30, y, -1)
            ElseIf vezY = 7 Then
                generadorLineaBlanca(5, 0, 15, y, -1)
            ElseIf vezY = 8 Then
                generadorLineaBlanca(4, 0, 0, y, -1)
            End If
        Next
    End Sub
    'carga cada linea del hexagono
    Private Sub generadorLineaBlanca(ByRef inicio As Integer, ByRef fin As Integer, ByRef retrocesoEnX As Integer, ByRef y As Integer, Optional ByRef paso As Integer = 1)
        For a = inicio To fin Step paso
            If mode3 = True Then
                Dim circuloBlancoX As New Ellipse
                circuloBlancoX.Width = 20
                circuloBlancoX.Stroke = Brushes.Black
                circuloBlancoX.Height = 20
                circuloBlancoX.Fill = Brushes.White
                AddHandler circuloBlancoX.MouseLeftButtonDown, AddressOf azulito
                Canvas.SetLeft(circuloBlancoX, ((130) - retrocesoEnX) + (30 * a))
                Canvas.SetTop(circuloBlancoX, y)
                pipo.Children.Add(circuloBlancoX)
                espacios(numero) = circuloBlancoX
                posicionesXEspacios(numero) = Canvas.GetLeft(circuloBlancoX)
                posicionesYEspacios(numero) = Canvas.GetTop(circuloBlancoX)
                numero += 1
                If contadorAgregar < 5 Then
                    Dim circuloBlanco As New Morado
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf azulito
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf radio
                    Canvas.SetLeft(circuloBlanco, (130 - retrocesoEnX) + (30 * a))
                    Canvas.SetTop(circuloBlanco, y)
                    listaMorado.Add(circuloBlanco)
                    pipo.Children.Add(circuloBlanco)
                    contadorAgregar += 1
                    fichas(numerox) = circuloBlanco
                    numerox += 1
                ElseIf contadorAgregar = 26 Or contadorAgregar = 42 Or contadorAgregar = 49 Or contadorAgregar = 55 Or contadorAgregar = 60 Then
                    Dim circuloBlanco As New Azul
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf azulito
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf radio
                    Canvas.SetLeft(circuloBlanco, (130 - retrocesoEnX) + (30 * a))
                    Canvas.SetTop(circuloBlanco, y)
                    listaAzul.Add(circuloBlanco)
                    pipo.Children.Add(circuloBlanco)
                    contadorAgregar += 1
                    fichas(numerox) = circuloBlanco
                    numerox += 1
                ElseIf contadorAgregar = 34 Or contadorAgregar = 35 Or contadorAgregar = 43 Or contadorAgregar = 50 Or contadorAgregar = 56 Then
                    Dim circuloBlanco As New Naranja
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf azulito
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf radio
                    Canvas.SetLeft(circuloBlanco, (130 - retrocesoEnX) + (30 * a))
                    Canvas.SetTop(circuloBlanco, y)
                    listaNaranja.Add(circuloBlanco)
                    pipo.Children.Add(circuloBlanco)
                    contadorAgregar += 1
                    fichas(numerox) = circuloBlanco
                    numerox += 1
                Else
                    Dim circuloBlanco As New Ellipse
                    circuloBlanco.Width = 20
                    circuloBlanco.Stroke = Brushes.Black
                    circuloBlanco.Height = 20
                    circuloBlanco.Fill = Brushes.White
                    AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf azulito
                    Canvas.SetLeft(circuloBlanco, (130 - retrocesoEnX) + (30 * a))
                    Canvas.SetTop(circuloBlanco, y)
                    pipo.Children.Add(circuloBlanco)
                    espacios(numero) = circuloBlanco
                    posicionesXEspacios(numero) = Canvas.GetLeft(circuloBlanco)
                    posicionesYEspacios(numero) = Canvas.GetTop(circuloBlanco)
                    numero += 1
                    contadorAgregar += 1
                End If
            Else
                Dim circuloBlanco As New Ellipse
                circuloBlanco.Width = 20
                circuloBlanco.Stroke = Brushes.Black
                circuloBlanco.Height = 20
                circuloBlanco.Fill = Brushes.White
                AddHandler circuloBlanco.MouseLeftButtonDown, AddressOf azulito
                Canvas.SetLeft(circuloBlanco, ((130) - retrocesoEnX) + (30 * a))
                Canvas.SetTop(circuloBlanco, y)
                pipo.Children.Add(circuloBlanco)
                espacios(numero) = circuloBlanco
                posicionesXEspacios(numero) = Canvas.GetLeft(circuloBlanco)
                posicionesYEspacios(numero) = Canvas.GetTop(circuloBlanco)
                numero += 1
            End If
        Next
    End Sub
    'prepara el tablero de forma inicial
    Private Sub Inicializacion(sender As Object, e As EventArgs)
        'ocultarJugadores()
        hexagono()
        For x = 0 To 41
            AddHandler espacios(x).MouseLeftButtonDown, AddressOf azulito
        Next
        Tablero()
    End Sub
    'analiza los movimientos y verifica que sean correctos y de serlos, los efectua
    Dim puntosParaAnalizar As New ArrayList
    Private Sub AgregarAEspaciosParaAnalizar(x As Double, y As Double)
        Dim puntoAnalizar As Point
        puntoAnalizar.X = x
        puntoAnalizar.Y = y
        If puntosParaAnalizar.Contains(puntoAnalizar) = False Then
            puntosParaAnalizar.Add(puntoAnalizar)
        End If
        
    End Sub
    Private Sub AgregarAEspaciosParaIgnorar(x As Double, y As Double)
        Dim puntoAnalizar As Point
        puntoAnalizar.X = x
        puntoAnalizar.Y = y
        If puntosPasados.Contains(puntoAnalizar) = False Then
            puntosPasados.Add(puntoAnalizar)
        End If
    End Sub
    Private Sub radio(ByVal elemento As Object, ñ As MouseEventArgs)
        actualizarLista()
        '----------------------------------------------------------------------COMPLETADO ----------------------------------------------------------------------------------------------------------------
        Dim p1 As Point
        p1.X = Canvas.GetLeft(elemento)
        p1.Y = Canvas.GetTop(elemento)
        For cont As Double = 0 To (espacios.Length - 1)
            If (p1.X + 35 >= posicionesXEspacios(cont)) And (p1.X - 35 <= posicionesXEspacios(cont)) And (p1.Y + 35 >= posicionesYEspacios(cont)) And (p1.Y - 35 <= posicionesYEspacios(cont)) Then
                '       espacios(cont).Fill = Brushes.Peru
            End If
        Next
        For Each pieza As Object In fichas
            'NE
            If ((K.X + 35 >= Canvas.GetLeft(pieza)) And (K.X + 5 < Canvas.GetLeft(pieza)) And (K.Y - 35 <= Canvas.GetTop(pieza) And (K.Y - 5 > Canvas.GetTop(pieza)))) Then
                For Each espacito As Ellipse In espacios
                    If (K.X + 50 >= Canvas.GetLeft(espacito)) And (K.X + 25 <= Canvas.GetLeft(espacito)) And (K.Y - 50 <= Canvas.GetTop(espacito)) And (K.Y - 35 >= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        'Exit For
                    End If
                Next
            ElseIf (K.X + 33 >= Canvas.GetLeft(pieza) And (K.X + 5 < Canvas.GetLeft(pieza)) And (K.Y + 33 >= Canvas.GetTop(pieza)) And (K.Y + 5 < Canvas.GetTop(pieza))) Then
                'SE
                For Each espacito As Ellipse In espacios
                    If (K.X + 40 >= Canvas.GetLeft(espacito)) And (K.X + 25 <= Canvas.GetLeft(espacito)) And (K.Y + 50 >= Canvas.GetTop(espacito)) And (K.Y + 35 <= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                    End If
                Next
                'Este
            ElseIf ((K.X + 35 >= Canvas.GetLeft(pieza) And (K.X < Canvas.GetLeft(pieza))) And (K.Y >= Canvas.GetTop(pieza)) And (K.Y <= Canvas.GetTop(pieza))) Then
                For Each espacito As Ellipse In espacios
                    If (K.X + 70 >= Canvas.GetLeft(espacito)) And (K.X + 30 <= Canvas.GetLeft(espacito)) And (K.Y >= Canvas.GetTop(espacito)) And (K.Y <= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                    End If
                Next
            ElseIf (K.X - 33 <= Canvas.GetLeft(pieza) And (K.X - 5 > Canvas.GetLeft(pieza)) And (K.Y - 35 <= Canvas.GetTop(pieza)) And (K.Y - 5 > Canvas.GetTop(pieza))) Then
                'NO
                For Each espacito As Ellipse In espacios
                    If (K.X - 50 <= Canvas.GetLeft(espacito)) And (K.X - 25 >= Canvas.GetLeft(espacito)) And (K.Y - 50 <= Canvas.GetTop(espacito)) And (K.Y - 25 >= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                    End If
                Next
            ElseIf (K.X - 35 <= Canvas.GetLeft(pieza) And (K.X - 5 > Canvas.GetLeft(pieza)) And (K.Y + 35 >= Canvas.GetTop(pieza)) And (K.Y + 5 < Canvas.GetTop(pieza))) Then
                'SO
                For Each espacito As Ellipse In espacios
                    If (K.X - 40 <= Canvas.GetLeft(espacito)) And (K.X - 30 >= Canvas.GetLeft(espacito)) And (K.Y + 50 >= Canvas.GetTop(espacito)) And (K.Y + 35 <= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                    End If
                Next
                'Oeste
            ElseIf ((K.X - 35 <= Canvas.GetLeft(pieza) And (K.X - 15 > Canvas.GetLeft(pieza))) And (K.Y >= Canvas.GetTop(pieza)) And (K.Y <= Canvas.GetTop(pieza))) Then
                For Each espacito As Ellipse In espacios
                    If (K.X - 70 <= Canvas.GetLeft(espacito)) And (K.X - 30 >= Canvas.GetLeft(espacito)) And (K.Y + 15 >= Canvas.GetTop(espacito)) And (K.Y - 15 <= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                    End If
                Next
            End If

            ' Exit For
            ' Next
            'verificarEspacios(p1)
            'For c = 0 To espaciosPosibles.Length - 1
            '    If espacios(cont).Equals(espaciosPosibles(c)) Then
            '        espacios(cont).Fill = Brushes.Red
            '    End If
            'Next
            ' ElseIf (p1.X + 35 >= posicionesXEspacios(cont)) And (p1.X - 35 <= posicionesXEspacios(cont)) And (p1.Y + 35 >= posicionesYEspacios(cont)) And (p1.Y - 35 <= posicionesYEspacios(cont)) Then
            '   espacios(cont).Fill = Brushes.RosyBrown
            ' End If
            ' 
        Next
        'For pos As Integer = 0 To puntosParaAnalizar.Count - 1
        '    cruzNazi(puntosParaAnalizar(pos))
        'Next
        For Each a As Ellipse In espacios
            Dim pun As New Point
            pun.Y = Canvas.GetTop(a)
            pun.X = Canvas.GetLeft(a)
            If puntosPasados.Contains(pun) = True Then
                cruzNazi(pun)
            End If
        Next
        Dim conter As Integer = 0
        Do While puntosParaAnalizar.Count >= puntosPasados.Count
            For Each q As Ellipse In espacios
                conter += 1
                If q.Fill.Equals(Brushes.Plum) Then
                    Dim ok As New Point
                    ok.X = Canvas.GetLeft(q)
                    ok.Y = Canvas.GetTop(q)
                    cruzNazi(ok)
                End If
                If puntosParaAnalizar.Count = 0 And puntosPasados.Count = 0 Or conter > 200 Then
                    Exit Do
                End If
            Next
        Loop
        Try
            For Each p In puntosParaAnalizar
                cruzNazi(p)
            Next

        Catch ex As Exception

        End Try
        ' Loop
        '  Loop
        '    MsgBox(espaciosParaAnalizar.Count)

        puntosParaAnalizar.Clear()
        puntosPasados.Clear()
        '    espaciosParaAnalizar.Clear()
        ReDim espaciosPosibles(0)
    End Sub
    Dim puntosPasados As New ArrayList
    Private Sub cruzNazi(punto As Point)
        'NE
        For Each pieza As Object In fichas
            If ((punto.X + 35 >= Canvas.GetLeft(pieza)) And (punto.X + 5 < Canvas.GetLeft(pieza)) And (punto.Y - 35 <= Canvas.GetTop(pieza) And (punto.Y - 5 > Canvas.GetTop(pieza)))) Then
                '  MsgBox("Espacio x " & Canvas.GetLeft(espacio) & " y: " & Canvas.GetTop(espacio))
                ' MsgBox("Ficha x " & Canvas.GetLeft(pieza) & " y: " & Canvas.GetTop(pieza))
                For Each espacito As Ellipse In espacios
                    ' Not (Canvas.GetLeft(pieza)) = Canvas.GetLeft(espacito) And Not (Canvas.GetTop(pieza)) = Canvas.GetTop(espacito) 
                    If (Not (punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And punto.X + 50 >= Canvas.GetLeft(espacito)) And (punto.X + 25 <= Canvas.GetLeft(espacito)) And (punto.Y - 50 <= Canvas.GetTop(espacito)) And (punto.Y - 25 >= Canvas.GetTop(espacito)) Then
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
            ElseIf (punto.X + 33 >= Canvas.GetLeft(pieza) And (punto.X + 5 < Canvas.GetLeft(pieza)) And (punto.Y + 33 >= Canvas.GetTop(pieza)) And (punto.Y + 5 < Canvas.GetTop(pieza))) Then
                'SE
                For Each espacito As Ellipse In espacios
                    If (Not (punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And punto.X + 50 >= Canvas.GetLeft(espacito)) And (punto.X + 25 <= Canvas.GetLeft(espacito)) And (punto.Y + 50 >= Canvas.GetTop(espacito)) And (punto.Y + 35 <= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
                'Este
            ElseIf ((punto.X + 35 >= Canvas.GetLeft(pieza) And (punto.X + 5 < Canvas.GetLeft(pieza))) And (punto.Y >= Canvas.GetTop(pieza)) And (punto.Y <= Canvas.GetTop(pieza))) Then
                For Each espacito As Ellipse In espacios
                    If (Not (punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And (punto.X + 70 >= Canvas.GetLeft(espacito)) And (punto.X + 30 <= Canvas.GetLeft(espacito)) And (punto.Y >= Canvas.GetTop(espacito)) And (punto.Y <= Canvas.GetTop(espacito))) Then
                        espacito.Fill = Brushes.Plum
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
            ElseIf (punto.X - 33 <= Canvas.GetLeft(pieza) And (punto.X - 5 > Canvas.GetLeft(pieza)) And (punto.Y - 35 <= Canvas.GetTop(pieza)) And (punto.Y - 5 > Canvas.GetTop(pieza))) Then
                'NO
                For Each espacito As Ellipse In espacios
                    If (Not (punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And punto.X - 50 <= Canvas.GetLeft(espacito)) And (punto.X - 25 >= Canvas.GetLeft(espacito)) And (punto.Y - 50 <= Canvas.GetTop(espacito)) And (punto.Y - 25 >= Canvas.GetTop(espacito)) Then
                        espacito.Fill = Brushes.Plum
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
            ElseIf (punto.X - 35 <= Canvas.GetLeft(pieza) And (punto.X - 5 > Canvas.GetLeft(pieza)) And (punto.Y + 35 >= Canvas.GetTop(pieza)) And (punto.Y + 5 < Canvas.GetTop(pieza))) Then
                'SO
                For Each espacito As Ellipse In espacios
                    If (Not punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And (punto.X - 40 <= Canvas.GetLeft(espacito)) And (punto.X - 25 >= Canvas.GetLeft(espacito)) And (punto.Y + 50 >= Canvas.GetTop(espacito)) And (punto.Y + 25 <= Canvas.GetTop(espacito)) Then
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
                'Oeste
            ElseIf ((punto.X - 35 <= Canvas.GetLeft(pieza)) And (punto.X - 5 > Canvas.GetLeft(pieza)) And (punto.Y >= Canvas.GetTop(pieza)) And (punto.Y <= Canvas.GetTop(pieza))) Then
                For Each espacito As Ellipse In espacios
                    If (Not (punto.X = Canvas.GetLeft(pieza)) And Not (punto.Y = Canvas.GetTop(pieza)) And punto.X - 70 <= Canvas.GetLeft(espacito)) And (punto.X - 30 >= Canvas.GetLeft(espacito)) And (punto.Y + 15 >= Canvas.GetTop(espacito)) And (punto.Y - 15 <= Canvas.GetTop(espacito)) Then
                        If espacito.Fill.Equals(Brushes.Plum) Then
                            AgregarAEspaciosParaIgnorar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        Else
                            espacito.Fill = Brushes.Plum
                            AgregarAEspaciosParaAnalizar(Canvas.GetLeft(espacito), Canvas.GetTop(espacito))
                        End If
                    End If
                Next
            End If
        Next
        'For Each neo As Ellipse In espaciosParaAnalizar
        '    If pipo.Children.Contains(neo) Then
        '        neo.Fill = Brushes.Red
        '    End If
        'Next
    End Sub
    'Private Sub radio(ByVal elemento As Object, ñ As MouseEventArgs)
    '    actualizarLista()

    '    '----------------------------------------------------------------------COMPLETADO ----------------------------------------------------------------------------------------------------------------
    '    '    Dim p1 As Point
    '    '   p1.X = Canvas.GetLeft(elemento)
    '    '  p1.Y = Canvas.GetTop(elemento)
    '    ' For cont As Double = 0 To (espacios.Length - 1)
    '    'If (p1.X + 60 >= posicionesXEspacios(cont)) And (p1.X - 60 <= posicionesXEspacios(cont)) And (p1.Y + 60 >= posicionesYEspacios(cont)) And (p1.Y - 60 <= posicionesYEspacios(cont)) Then
    '    For Each pieza As Object In fichas
    '        'NE
    '        If ((K.X + 35 >= Canvas.GetLeft(pieza)) And (K.X + 0 < Canvas.GetLeft(pieza)) And (K.Y - 35 <= Canvas.GetTop(pieza) And (K.Y - 0 > Canvas.GetTop(pieza)))) Then
    '            For Each espacito As Ellipse In espacios
    '                If (K.X + 50 >= Canvas.GetLeft(espacito)) And (K.X + 25 <= Canvas.GetLeft(espacito)) And (K.Y - 50 <= Canvas.GetTop(espacito)) And (K.Y - 25 >= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                    'Exit For
    '                End If
    '            Next
    '        ElseIf (K.X + 33 >= Canvas.GetLeft(pieza) And (K.X < Canvas.GetLeft(pieza)) And (K.Y + 33 >= Canvas.GetTop(pieza)) And (K.Y + 10 < Canvas.GetTop(pieza))) Then
    '            'SE
    '            For Each espacito As Ellipse In espacios
    '                If (K.X + 40 >= Canvas.GetLeft(espacito)) And (K.X + 25 <= Canvas.GetLeft(espacito)) And (K.Y + 50 >= Canvas.GetTop(espacito)) And (K.Y + 25 <= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.Add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                End If
    '            Next
    '        ElseIf ((K.X + 35 >= Canvas.GetLeft(pieza) And (K.X < Canvas.GetLeft(pieza))) And (K.Y >= Canvas.GetTop(pieza)) And (K.Y <= Canvas.GetTop(pieza))) Then
    '            For Each espacito As Ellipse In espacios
    '                If (K.X + 70 >= Canvas.GetLeft(espacito)) And (K.X + 30 <= Canvas.GetLeft(espacito)) And (K.Y + 15 >= Canvas.GetTop(espacito)) And (K.Y - 15 <= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.Add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                End If
    '            Next
    '        ElseIf (K.X - 33 <= Canvas.GetLeft(pieza) And (K.X - 0 > Canvas.GetLeft(pieza)) And (K.Y - 35 <= Canvas.GetTop(pieza)) And (K.Y - 0 > Canvas.GetTop(pieza))) Then
    '            'NO
    '            For Each espacito As Ellipse In espacios
    '                If (K.X - 40 <= Canvas.GetLeft(espacito)) And (K.X - 25 >= Canvas.GetLeft(espacito)) And (K.Y - 50 <= Canvas.GetTop(espacito)) And (K.Y - 25 >= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.Add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                End If
    '            Next
    '        ElseIf (K.X - 35 <= Canvas.GetLeft(pieza) And (K.X > Canvas.GetLeft(pieza)) And (K.Y + 35 >= Canvas.GetTop(pieza)) And (K.Y < Canvas.GetTop(pieza))) Then
    '            'SO
    '            For Each espacito As Ellipse In espacios
    '                If (K.X - 40 <= Canvas.GetLeft(espacito)) And (K.X - 25 >= Canvas.GetLeft(espacito)) And (K.Y + 50 >= Canvas.GetTop(espacito)) And (K.Y + 25 <= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.Add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                End If
    '            Next
    '            'Oeste
    '        ElseIf ((K.X - 35 <= Canvas.GetLeft(pieza) And (K.X > Canvas.GetLeft(pieza))) And (K.Y >= Canvas.GetTop(pieza)) And (K.Y <= Canvas.GetTop(pieza))) Then
    '            For Each espacito As Ellipse In espacios
    '                If (K.X - 70 <= Canvas.GetLeft(espacito)) And (K.X - 30 >= Canvas.GetLeft(espacito)) And (K.Y + 15 >= Canvas.GetTop(espacito)) And (K.Y - 15 <= Canvas.GetTop(espacito)) Then
    '                    espaciosParaAnalizar.Add(espacito)
    '                    espacito.Fill = Brushes.Plum
    '                End If
    '            Next
    '        End If
    '        ' Exit For
    '        ' Next
    '        'verificarEspacios(p1)
    '        'For c = 0 To espaciosPosibles.Length - 1
    '        '    If espacios(cont).Equals(espaciosPosibles(c)) Then
    '        '        espacios(cont).Fill = Brushes.Red
    '        '    End If
    '        'Next
    '        ' ElseIf (p1.X + 35 >= posicionesXEspacios(cont)) And (p1.X - 35 <= posicionesXEspacios(cont)) And (p1.Y + 35 >= posicionesYEspacios(cont)) And (p1.Y - 35 <= posicionesYEspacios(cont)) Then
    '        '   espacios(cont).Fill = Brushes.RosyBrown
    '        ' End If
    '        ' 
    '    Next
    '    MsgBox(espaciosParaAnalizar.Count)
    '    espaciosParaAnalizar.Clear()
    '    ReDim espaciosPosibles(0)
    'End Sub
    Private Sub verificarEspacios(k As Point)
        contadorTamanio = 0
        For cont As Double = 0 To (espacios.Length - 1)
            'Radio Mayor
            If (k.X + 70 >= posicionesXEspacios(cont)) And (k.X - 70 <= posicionesXEspacios(cont)) And (k.Y + 70 >= posicionesYEspacios(cont)) And (k.Y - 70 <= posicionesYEspacios(cont)) Then
                For Each pieza As Object In fichas
                    'NE
                    If ((k.X + 35 >= Canvas.GetLeft(pieza)) And (k.Y - 35 <= Canvas.GetTop(pieza))) Then
                        For Each espacito As Ellipse In espacios
                            espacito.Fill = Brushes.Plum
                        Next
                        'NO
                    ElseIf (k.X - 35 <= Canvas.GetLeft(pieza) And (k.Y - 35) <= Canvas.GetTop(pieza)) Then
                        'SE
                        For Each espacito As Ellipse In espacios
                            espacito.Fill = Brushes.Plum
                        Next
                    ElseIf (k.X + 35 >= Canvas.GetLeft(pieza) And (k.Y + 35 <= Canvas.GetTop(pieza))) Then
                        'SO
                        For Each espacito As Ellipse In espacios
                            espacito.Fill = Brushes.Plum
                        Next
                    ElseIf (k.X - 35 <= Canvas.GetLeft(pieza) And (k.Y + 35) <= Canvas.GetTop(pieza)) Then
                        For Each espacito As Ellipse In espacios
                            espacito.Fill = Brushes.Plum
                        Next
                    End If
                Next
                'si punto entre punto A hasta el punto C encontramos un punto B y este es una ficha
                'Si necesito que sea entonces entre el punto A y el punto C
                '¿como obtengo eso?
                'AX +35 >= posicionesXEspacios(cont)
                espaciosPosibles(contadorTamanio) = espacios(cont)
                ReDim Preserve espaciosPosibles(contadorTamanio + 1)
                contadorTamanio += 1
                cont += 1
            End If
        Next
    End Sub
    Dim contadorSegunJugadores As Integer = 0
    Dim turno As Integer = 1

    Private Function verificarFichas(k As Point) As Object
        For cont As Double = 0 To (fichas.Length - 1)
            If (k.X + 35 >= posicionesXFichas(cont)) And (k.X - 35 <= posicionesXFichas(cont)) And (k.Y + 35 >= posicionesYFichas(cont)) And (k.Y - 35 <= posicionesYFichas(cont)) And Not (posicionesXFichas(cont) = Canvas.GetLeft(ficha)) And Not (posicionesYFichas(cont) = Canvas.GetTop(ficha)) Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Sub azulito(sender As Object, e As MouseEventArgs)
        '   MsgBox("X : " & Canvas.GetLeft(sender) & " Y... " & Canvas.GetTop(sender))
        If ventanaNombres3.de3A6 = True Then
            contadorSegunJugadores = 2
            juego3 = True
            juego6 = False
            lblp1.Visibility = Windows.Visibility.Visible
            lblp2.Visibility = Windows.Visibility.Visible
            lblp3.Visibility = Windows.Visibility.Visible
            c1.Visibility = Windows.Visibility.Visible
            c2.Visibility = Windows.Visibility.Visible
            c2.Fill = Brushes.Orange
            c3.Fill = Brushes.SkyBlue
            c3.Visibility = Windows.Visibility.Visible
            lblp1.Content = ventanaNombres3.nm1
            lblp2.Content = ventanaNombres3.nm2
            lblp3.Content = ventanaNombres3.nm3
            lblp4.Visibility = Windows.Visibility.Hidden
            lblp5.Visibility = Windows.Visibility.Hidden
            lblp6.Visibility = Windows.Visibility.Hidden
            c4.Visibility = Windows.Visibility.Hidden
            c5.Visibility = Windows.Visibility.Hidden
            c6.Visibility = Windows.Visibility.Hidden
        ElseIf ventanaNombres6.afirmativo = 1 Then
            If ventanaNombres6.de6A3 = True Then
                contadorSegunJugadores = 2
                juego3 = True
                juego6 = False
                If ventanaNombres6.n1 Then
                    c1.Visibility = Windows.Visibility.Hidden
                    c1.Fill = Brushes.Purple
                    lblp1.Visibility = Windows.Visibility.Visible
                    lblp1.Content = ventanaNombres6.nm1
                End If
                If ventanaNombres6.n2 Then
                    c2.Visibility = Windows.Visibility.Hidden
                    c2.Fill = Brushes.Yellow
                    lblp2.Visibility = Windows.Visibility.Visible
                    lblp2.Content = ventanaNombres6.nm2
                End If
                If ventanaNombres6.n3 Then
                    c3.Visibility = Windows.Visibility.Hidden
                    c3.Fill = Brushes.Orange
                    lblp3.Visibility = Windows.Visibility.Visible
                    lblp3.Content = ventanaNombres6.nm3
                End If
                If ventanaNombres6.n4 Then
                    c4.Visibility = Windows.Visibility.Hidden
                    c4.Fill = Brushes.Red
                    lblp4.Visibility = Windows.Visibility.Visible
                    lblp4.Content = ventanaNombres6.nm4
                End If
                If ventanaNombres6.n5 Then
                    c5.Visibility = Windows.Visibility.Hidden
                    c5.Fill = Brushes.SkyBlue
                    lblp5.Visibility = Windows.Visibility.Visible
                    lblp5.Content = ventanaNombres6.nm5
                End If
                If ventanaNombres6.n6 Then
                    c6.Visibility = Windows.Visibility.Hidden
                    c6.Fill = Brushes.Green
                    lblp6.Visibility = Windows.Visibility.Visible
                    lblp6.Content = ventanaNombres6.nm6
                End If
                If ventanaNombres6.n1 = False Then
                    c1.Visibility = Windows.Visibility.Hidden
                    c1.Fill = Brushes.Purple
                    lblp1.Visibility = Windows.Visibility.Hidden
                    c1.Visibility = Windows.Visibility.Hidden
                End If
                If ventanaNombres6.n2 = False Then
                    c2.Visibility = Windows.Visibility.Hidden
                    c2.Fill = Brushes.Yellow
                    lblp2.Visibility = Windows.Visibility.Hidden
                    c2.Visibility = Windows.Visibility.Hidden
                End If
                If ventanaNombres6.n3 = False Then
                    c3.Fill = Brushes.Orange
                    lblp3.Visibility = Windows.Visibility.Hidden
                    c3.Visibility = Windows.Visibility.Hidden
                End If
                If ventanaNombres6.n4 = False Then
                    lblp4.Visibility = Windows.Visibility.Hidden
                    c4.Visibility = Windows.Visibility.Hidden
                    c4.Fill = Brushes.Red
                End If
                If ventanaNombres6.n5 = False Then
                    lblp5.Visibility = Windows.Visibility.Hidden
                    c5.Visibility = Windows.Visibility.Hidden
                    c5.Fill = Brushes.SkyBlue
                End If
                If ventanaNombres6.n6 = False Then
                    lblp6.Visibility = Windows.Visibility.Hidden
                    c6.Visibility = Windows.Visibility.Hidden
                    c6.Fill = Brushes.Green
                End If
            ElseIf ventanaNombres6.de6A3 = False Then
                contadorSegunJugadores = 1
                juego6 = True
                juego3 = False
                c1.Fill = Brushes.Purple
                c2.Fill = Brushes.Yellow
                c3.Fill = Brushes.Orange
                c4.Fill = Brushes.Red
                c5.Fill = Brushes.SkyBlue
                c6.Fill = Brushes.Green
                lblp1.Visibility = Windows.Visibility.Visible
                lblp2.Visibility = Windows.Visibility.Visible
                lblp3.Visibility = Windows.Visibility.Visible
                lblp4.Visibility = Windows.Visibility.Visible
                lblp5.Visibility = Windows.Visibility.Visible
                lblp6.Visibility = Windows.Visibility.Visible
                lblp1.Content = ventanaNombres6.nm1
                lblp2.Content = ventanaNombres6.nm2
                lblp3.Content = ventanaNombres6.nm3
                lblp4.Content = ventanaNombres6.nm4
                lblp5.Content = ventanaNombres6.nm5
                lblp6.Content = ventanaNombres6.nm6
            End If
        Else
        End If
        Dim mor As New Morado
        Dim ama As New Amarillo
        Dim nar As New Naranja
        Dim roj As New rojo
        Dim azu As New Azul
        Dim ver As New Verde
        Try
            If Not (sender.fill.Equals(Brushes.Black)) Then
            End If
        Catch ex As Exception
            If juego3 = True Then
                If listaMorado.Contains(sender) And (turno = 1) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 4
                ElseIf listaNaranja.Contains(sender) And (turno = 4) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 6
                ElseIf listaAzul.Contains(sender) And (turno = 6) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 1
                Else
                    MsgBox("No es tu turno")
                    K.X = 0
                    K.Y = 0
                End If
            ElseIf juego6 = True Then
                If listaMorado.Contains(sender) And (turno = 1) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 2
                ElseIf listaAmarillo.Contains(sender) And (turno = 2) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 3
                ElseIf listaNaranja.Contains(sender) And (turno = 3) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 4
                ElseIf listaRojo.Contains(sender) And (turno = 4) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 5
                ElseIf listaAzul.Contains(sender) And (turno = 5) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 6
                ElseIf listaVerde.Contains(sender) And (turno = 6) Then
                    ficha = sender
                    K.X = Canvas.GetLeft(sender)
                    K.Y = Canvas.GetTop(sender)
                    turno = 1
                Else
                    MsgBox("No es tu turno")
                    K.X = 0
                    K.Y = 0
                End If
            End If
        End Try
        Try
            If Not (sender.fill.Equals(Brushes.Black)) And Not (K.X = 0) And Not (K.Y = 0) Then
                ficha2 = sender
                L.X = Canvas.GetLeft(sender)
                L.Y = Canvas.GetTop(sender)
                blanquear()
            Else
                contador = 0
            End If
        Catch ex As Exception
        End Try
        If Not (K.X = 0) And Not (K.Y = 0) And Not (L.Y = 0) And Not (L.X = 0) Then
            If (L.X - K.X >= -33.5) And (L.Y - K.Y >= -33.5) And (L.X - K.X <= 33.5) And (L.Y - K.Y <= 33.5) Then
                pipo.Children.Remove(ficha)
                Canvas.SetLeft(ficha, L.X)
                Canvas.SetTop(ficha, L.Y)
                pipo.Children.Add(ficha)
                K.X = 0
                K.Y = 0
                L.X = 0
                L.Y = 0
                contadorUniversal = contadorUniversal + contadorSegunJugadores
                actualizarLista()
                blanquear()
            Else
                If verificarFichas(L) Then
                    ' Do While K.X = K.X + 35 And K.Y = K.Y + 35
                    Do While verificarFichas(K)
                        K.X = K.X + 35
                        K.Y = K.Y + 35
                        If verificarFichas(K) = False Then
                            pipo.Children.Remove(ficha)
                            Canvas.SetLeft(ficha, L.X)
                            Canvas.SetTop(ficha, L.Y)
                            pipo.Children.Add(ficha)
                            Exit Do
                        End If
                    Loop
                    ' Loop
                End If
                'If verificarFichas(L) Then
                '    Dim particionesX As Integer = Fix(L.X / 35)
                '    Dim particionesY As Integer = Fix(L.Y / 35)
                '    Dim listadoX(particionesX) As Double
                '    Dim listadoY(particionesY) As Double
                '    Dim vuelta As Integer = 0
                '    Dim var As Integer = 1
                '    For x = 0 To particionesX
                '        listadoX(x) = L.X / var
                '        listadoY(x) = L.Y / var
                '        If x = 0 Then
                '            var = 0
                '        End If
                '        var += part
                '    Next
                '    Do While vuelta < particionesX
                '        If verificarFichas(L) Then
                '            pipo.Children.Remove(ficha)
                '            Canvas.SetLeft(ficha, L.X)
                '            Canvas.SetTop(ficha, L.Y)
                '            pipo.Children.Add(ficha)
                '        Else
                '            MsgBox("No se puede")
                '            Return
                '        End If
                '        particionesX += 35
                '        vuelta += 1
                '    Loop
                '    K.X = 0
                '    K.Y = 0
                '    L.X = 0
                '    L.Y = 0
                '    actualizarLista()
                '    blanquear()
                ' End If
            End If
            K.X = 0
            K.Y = 0
            L.X = 0
            L.Y = 0
        ElseIf Not (Ñ.X = 0) And Not (Ñ.Y = 0) And Not (K.X = 0) And Not (K.Y = 0) Then
            actualizarLista()
        End If
        If juego3 = True Then
            revision()
        ElseIf juego6 Then
            revision3()
        End If
    End Sub
    Public Sub revision()
        Dim x As Double
        Dim y As Double
        For Each m As Morado In listaMorado
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 144 And x < 236 And y > 284 And y < 346 Then
                MsgBox("Ganaste Morado!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As rojo In listaRojo
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 144 And x < 236 And y > 24 And y < 86 Then
                MsgBox("Ganaste Rojo!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Amarillo In listaAmarillo
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 11 And x < 103 And y > 104 And y < 166 And Not (x = 100 And y = 225) And Not (x = 85 And y = 205) Then
                MsgBox("Ganaste Amarillo!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Naranja In listaNaranja
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 11 And x < 103 And y > 204 And y < 266 And Not (x = 100 And y = 145) And Not (x = 85 And y = 165) Then
                MsgBox("Ganaste Naranja!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Azul In listaAzul
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 277 And x < 369 And y > 104 And y < 166 And Not (x = 280 And y = 145) And Not (x = 295 And y = 165) Then
                MsgBox("Ganaste Azul!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Verde In listaVerde
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 277 And x < 369 And y > 204 And y < 266 And Not (x = 280 And y = 225) And Not (x = 295 And y = 205) Then
                MsgBox("Ganaste Verde!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
    End Sub
    Public Sub revision3()
        Dim x As Double
        Dim y As Double
        For Each m As Morado In listaMorado
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 129 And x < 251 And y > 264 And y < 346 Then
                MsgBox("Ganaste Morado!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Naranja In listaNaranja
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 11 And x < 106 And y > 104 And y < 185 And Not (x = 130 And y = 145) And Not (x = 130 And y = 185) And Not (x = 115 And x = 165) And Not (x = 100 And y = 185) Then
                MsgBox("Ganaste Naranja!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
        For Each m As Azul In listaAzul
            x = Canvas.GetLeft(m)
            y = Canvas.GetTop(m)
            If x > 249 And x < 369 And y > 104 And y < 186 And Not (x = 250 And y = 145) And Not (x = 250 And y = 185) And Not (x = 265 And x = 165) And Not (x = 280 And y = 185) Then
                MsgBox("Ganaste Azul!")
                MsgBox("Para jugar de nuevo favor dar click en 'nuevo juego'")
                Exit For
            End If
        Next
    End Sub

    Private Sub blanquear()
        For Each a As Ellipse In espacios
            a.Fill = Brushes.White
        Next
    End Sub
    'lleva registro de la posicion actual de las fichas
    Private Sub actualizarLista()
        contador = 0
        If mode3 = True Then
            ReDim Preserve fichas(44)
            ReDim posicionesXFichas(44)
            ReDim posicionesYFichas(44)
            ReDim Preserve posicionesXEspacios(120)
            ReDim Preserve posicionesYEspacios(120)
            ReDim Preserve espacios(120)
        Else
            ReDim posicionesXFichas(59)
            ReDim posicionesYFichas(59)
        End If
        For t = 0 To fichas.Length - 1
            posicionesYFichas(t) = Canvas.GetTop(fichas(t))
            posicionesXFichas(t) = Canvas.GetLeft(fichas(t))
        Next
    End Sub
End Class